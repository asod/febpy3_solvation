import numpy as np
from ase.io import read
from ase import Atoms
from cmm.md.cmmsystems import CMMSystem
from parmed.openmm.reporters import RestartReporter
from simtk.unit import angstroms 
from simtk import unit as omm_u
from ase import units as ase_u
''' An example of running OpenMM MD, after completing the parametrisation
    using e.g. MCPB.py of ambertools. 
    Needs https://gitlab.com/asod/cmmtools    
'''

tag = 'febpy_gs_opls_frzn'
fpath = './'


def nonsolvent_as_ase(sys, ns_idx):
    _, syms = sys.get_nonsolvent_indices(tag='Cl-')
    els = [atom.element._symbol for a, atom in enumerate(sys.topology.atoms()) if a in ns_idx]
    pos = np.array(sys.positions.value_in_unit(angstroms))[ns_idx]
    atoms  = Atoms(els, positions=pos)
    return atoms

# init
sys = CMMSystem(tag, fpath=fpath)

# load prmtop: 
sys.system_from_prmtop('FEBPY_solv.prmtop', inpcrd='FEBPY_solv.inpcrd')


## The new FF has reindexed itself, so we need the map to apply the right charges
# to the right atoms.
# The structure the charges belong to
qm_atoms = read('../geometries/febpy3/febpy3_ls_b3lypd3bj_def2-TZVP.xyz')

mm_atoms = nonsolvent_as_ase(sys, list(range(61)))
qm_atoms.positions += qm_atoms.positions[0] + mm_atoms.positions[0] # this is enough for aligning

charges = np.genfromtxt('../charges.dat', skip_header=2, skip_footer=5)[:, 1]

new_pos = qm_atoms.get_positions()
new_syms = qm_atoms.get_chemical_symbols()
index_map = [] # first metal is the same
for o, oa in enumerate(mm_atoms):
    old_sym = oa.symbol
    dists = np.linalg.norm(new_pos - oa.position, axis=1)  # all dists from oa to kruppa
    # find the shortest distance to an atom OF THE SAME ELEMENT   
    idx = np.ma.argmin(np.ma.MaskedArray(dists, [ns != old_sym for ns in new_syms]), fill_value=100)
    index_map.append(idx)
    print(f'old atom {o}, {old_sym} is closest to new {idx}, {new_syms[idx]}, distance: {dists[idx]}')


ns_idx, syms = sys.get_nonsolvent_indices(tag='Cl-')
solu_idx = [i for i, sym in enumerate(syms) if sym != 'Cl-']
sys.adjust_solute_charges(solu_idx, charges[index_map])

# Change LJs to OPLS 
opls = {'C':  (0.00329567 / (ase_u.kcal / ase_u.mol) * omm_u.kilocalorie_per_mole, 3.55000 * omm_u.angstrom), 
        'N':  (0.00737190 / (ase_u.kcal / ase_u.mol) * omm_u.kilocalorie_per_mole, 3.25000 * omm_u.angstrom),
        'FE': (0.00056373 / (ase_u.kcal / ase_u.mol) * omm_u.kilocalorie_per_mole, 2.59400 * omm_u.angstrom),
        'H':  (2.00130092 / (ase_u.kcal / ase_u.mol) * omm_u.kilocalorie_per_mole, 2.42000 * omm_u.angstrom)}

sys.adjust_lj(solu_idx, opls)

# Restrain FeBPY to the positions from the opt, and move Cl- away
ct_idx = [i for i, sym in enumerate(syms) if sym == 'Cl-']
solu_pos = sys.positions[solu_idx].value_in_unit(angstroms)
ct_pos  = sys.positions[ct_idx].value_in_unit(angstroms)
cen = np.mean(solu_pos, 0)
vs = ct_pos - cen
vs /= np.linalg.norm(vs, axis=1)[:, None]
for i, v in enumerate(vs):
    print(np.linalg.norm(ct_pos[i] - cen, axis=0))
    ct_pos[i] += 15 * v
    print(np.linalg.norm(ct_pos[i] - cen, axis=0))

pos = sys.positions.value_in_unit(angstroms)
pos[ct_idx] = ct_pos
pos[solu_idx] = qm_atoms.positions[index_map]

sys.update_positions(pos)
sys.restrain_atoms(ns_idx, 500)

# this time, no charge changing, or adding constraints. So init NPT:
sys.add_barostat()

# and integrator
sys.set_integrator()  

# init sim
sys.init_simulation(platform='CUDA', double=False)

# minimize
sys.minimize()

# MBD:
sys.mbd()

# reporters
sys.standard_reporters(step=250)

restrt = RestartReporter(fpath + f'{tag}.rst7', 1000)
sys.add_reporter(restrt)

# runnnn
sys.run(time_in_ps=15000)
