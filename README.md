Simulating the Solvation Structure of Low- and High-Spin [Fe(bpy)<sub>3</sub>]<sup>2+</sup>: Long-Range Dispersion and Many-Body Effects
==========================================================================================================================
![B3LYP-D3(BJ) Optimized 17-Water Shell Geometry](./viz/qm17shell_lbl.gif)

Data Repository for the work of the paper of the same name. 

Contents:

Code:
-----
Scripts to enable ASE to do geometry optimizations using FFs, and to automate 3-body interaction energy calculations.

Data:
-----
Containing data and small plot-scripts to reproduce all non-illustration figures from the paper.

Geometries:
-----------
xyz files of all molecular geometries used in the study. 


This dataset has the doi: 10.5281/zenodo.6599115

