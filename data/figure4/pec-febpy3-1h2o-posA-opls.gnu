set size 1,1
set encoding iso_8859_1
# set title "pose A"
set output "pec-febpy3-1h2o-posA-opls.eps"
set terminal postscript eps enhanced color font ",22"
set ylabel "V (eV)" offset 1
set xlabel "Fe-O distance ({\305})"
set yrange [-0.35:0.35]
set xrange [3.5:15.0]
set xtics font ",22"
set ytics font ",22"
set format y "%1.1f"
set format x "%1.1f"
# set key left bottom
# set label "Methanol" at 0,1.47
set arrow from 3.5,0 to 8.0,0 nohead 
plot "./pec-febpy3-1h2o-posA-singlet-opls.dat"                 using 1:($2+$3) w l title "OPLS LS" lt 1 lw 4 lc 3 ,\
     "./pec-febpy3-1h2o-posA-quintet-opls.dat"                 using 1:($2+$3) w l title "OPLS HS" lt 1 lw 4 lc 1 ,\
     "./pec-febpy3-1h2o-posA-singlet-dlpno-ccsdt-def2tzvp.dat" using 1:2  w p title "DLPNO-CCSD(T) LS" pt 1 ps 2 lw 4 lc 3 ,\
     "./pec-febpy3-1h2o-posA-quintet-dlpno-ccsdt-def2tzvp.dat" using 1:2  w p title "DLPNO-CCSD(T) HS" pt 2 ps 2 lw 4 lc 1   
