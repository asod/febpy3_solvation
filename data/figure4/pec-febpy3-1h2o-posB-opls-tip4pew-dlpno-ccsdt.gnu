set size 1,1
set encoding iso_8859_1
set output "pec-febpy3-1h2o-posB-opls-tip4pew-dlpno-ccsdt.eps"
set terminal postscript eps enhanced color font ",24"
set ylabel "V (eV)" offset 1
set xlabel "Fe-O distance ({\305})"
set yrange [-0.4:0.6]
set xrange [3.5:8.0]
set xtics font ",26"
set ytics font ",26"
set format y "%1.1f"
set format x "%1.1f"
set arrow from 3.5,0 to 8.0,0 nohead 
set label "pose B" at 7.0,-0.31 font ",32"
plot "pec-febpy3-1h2o-posB-opls-tip4pew.dat" u 1:2 w l title "OPLS/TIP4PEW LS" lt 1 lw 4 lc 3 ,\
     "pec-febpy3-1h2o-posB-opls-tip4pew.dat" u 1:3 w l title "OPLS/TIP4PEW HS" lt 1 lw 4 lc 1 ,\
     "pec-febpy3-1h2o-posB-dlpno-ccsdt.dat"  u 1:2 w p title "DLPNO-CCSD(T) LS" pt 1 ps 2 lw 4 lc 3 ,\
     "pec-febpy3-1h2o-posB-dlpno-ccsdt.dat"  u 1:3 w p title "DLPNO-CCSD(T) HS" pt 2 ps 2 lw 4 lc 1   
