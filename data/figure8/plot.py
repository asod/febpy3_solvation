import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{cmbright}')

plt.rcParams.update({'font.size': 18})

daku2018 = np.genfromtxt('daku2018_febpy_FeO_LS.csv', delimiter=',')
sort = np.argsort(daku2018[:, 0])
daku2018 = daku2018[sort, :]

khak_ls_co = np.genfromtxt('khakhulin2019_LS_CO.csv', delimiter=',')
sort = np.argsort(khak_ls_co[:, 0])
khak_ls_co = khak_ls_co[sort, :]


rdf_opls = np.load('opls_febpy_FeO.npy')
opls_ls_co = np.load('opls_febpy_CO.npy')

aimdbox = np.genfromtxt('aimdbox_Fe_u-O_v.dat')
aimdbox_noct = np.genfromtxt('aimdbox_noct_Fe_u-O_v.dat')
bigbox = np.genfromtxt('bigbox_Fe_u-O_v.dat')  # same size as the tleap box but made in Packmol


fig, axes = plt.subplots(2, 1, figsize=(7, 10))
ax = axes[0]
ax.plot([0, 30], [1, 1], 'k--')
ax.plot(rdf_opls[:, 0], rdf_opls[:, 1], 'k-')
ax.plot(aimdbox[:, 0], aimdbox[:, 1], 'C4-')
ax.plot(aimdbox_noct[:, 0], aimdbox_noct[:, 1], 'C4--')
ax.plot(daku2018[:, 0], daku2018[:, 1], 'C3')
ax.set_ylabel('Fe-O g(r)')
ax.set_ylim([0, 1.5])
ax.set_xticklabels([])


### C-O
aimdbox = np.genfromtxt('aimdbox_C_u-O_v.dat')
aimdbox_noct = np.genfromtxt('aimdbox_noct_C_u-O_v.dat')
ax = axes[1]
ax.plot([0, 25], [1, 1], 'k--')
ax.plot(opls_ls_co[:, 0], opls_ls_co[:, 1], 'k-', label='$\sim$221 nm$^3$ box,  OPLS/TIP4PEW')
ax.plot(aimdbox[:, 0], aimdbox[:, 1], 'C4-', label='9.3 nm$^3$ box, OPLS/TIP4PEW')
ax.plot(aimdbox_noct[:, 0], aimdbox_noct[:, 1], 'C4--', label='9.3 nm$^3$ box, OPLS/TIP4PEW, no Cl$^-$')
ax.plot(khak_ls_co[:, 0], khak_ls_co[:, 1], 'C3', label='9.3 nm$^3$ box, BLYPD3')

ax.set_ylabel('C-O g(r)')
ax.legend(loc='best', fontsize=17)
ax.set_ylabel('C-O g(r)')
ax.set_ylim([0, 1.199])
ax.set_xlabel('r (Å)')
for ax in axes:
    ax.grid()
    ax.set_xlim([2.0, 14])
    
fig.tight_layout(h_pad=-0.2)
plt.savefig('fig8.pdf', bbox_inches='tight', dpi=300)
plt.show()
