import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rc('text', usetex=True)
matplotlib.rc('text.latex', preamble=r'\usepackage{cmbright}')
plt.rcParams.update({'font.size': 18})
from ase.io import read

hs = np.load('febpy3_17qm_hbond_analys_of_opls_hs_md_w3.4.npy', allow_pickle=True).item()
ls = np.load('febpy3_17qm_hbond_analys_of_opls_ls_md_w3.4.npy', allow_pickle=True).item()
pure_tip4pew = np.load('pure_tip4pew.npy', allow_pickle=True).item()

plt.rcParams.update({'font.size': 14})
qm17_poses = {88: 'D', 79: 'B', 82: 'B', 85: 'B', 100: 'C', 97: 'C', 106: 'C', 67: 'A', 61: 'A', 64: 'A',
              109: 'C', 103:'C', 94: 'C', 76: 'B', 73: 'B', 70: 'B', 91: 'D'}

qm_ls = read('../../geometries/febpy3-17-20h2o/febpy3-17h2o_b3lypd3bj_def2-TZVP_ls.xyz') 
qm_hs = read('../../geometries/febpy3-17-20h2o/febpy3-17h2o_b3lypd3bj_def2-TZVP_hs.xyz') 
mm_ls = read('../../geometries/febpy3-17-20h2o/febpy3-17h2o_opls_q-sol_ls.xyz') 
mm_hs = read('../../geometries/febpy3-17-20h2o/febpy3-17h2o_opls_q-vac_hs.xyz') 

qms = []
for qm_atoms in (qm_ls, qm_hs):
    qm_distances = {'A': [], 'B': [], 'C': [], 'D': []}
    for oidx in sorted(qm17_poses.keys()):
        d = qm_atoms.get_distance(0, oidx)
        qm_distances[qm17_poses[oidx]].append(d)
    qms.append(qm_distances)


mms = []
for mm_atoms in (mm_ls, mm_hs):
    mm_distances = {'A': [], 'B': [], 'C': [], 'D': []}
    for oidx in sorted(qm17_poses.keys()):  # same indexing, was optimized from the QM shell..
        d = mm_atoms.get_distance(0, oidx)
        mm_distances[qm17_poses[oidx]].append(d)
    mms.append(mm_distances)
    
    

cols = ['C0', 'C3']
xmin = 4.0
xmax = 7.5
nbins = 50
x = np.linspace(xmin, xmax, nbins)
dx = x[1] - x[0]
fig, axes = plt.subplots(4, 2, figsize=(8, 7))    
for i, (key, d) in enumerate(ls['pose_feos'].items()):
    if key == 'Unlabeled':
        continue
    ax = axes[i, 0]
    h, x = np.histogram(d, x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h, f'C0-', mfc='None')
        
    h, x = np.histogram(hs['pose_feos'][key], x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h, f'C3', mfc='None')

    #ax.legend(loc='best', fontsize=24)
    ax.set_xlim([4, 6.999])
    ax.grid()
    if i < 3:
        ax.set_xticklabels([])
    ax.set_ylabel('$\Gamma(d_\mathrm{FeO})$ (Å$^-1$)')
    ax.set_ylim([0, 0.95])
    ax.text(6.3, 0.78, f'Pose {key}', fontweight='bold', fontsize=16)
    ax.set_xticks(np.arange(4, 7, 0.5))

    
    for q, qm in enumerate(qms):
        ax.axvline(np.mean(qm[key]), color=cols[q])
    for m, mm in enumerate(mms):
        ax.axvline(np.mean(mm[key]), color=cols[m], linestyle='--')
    
ax.set_xlabel('$d_\mathrm{FeO}$ (Å)')

xmin = 50
xmax = 190
nbins = 50
x = np.linspace(xmin, xmax, nbins)
dx = x[1] - x[0]
for i, (key, d) in enumerate(ls['pose_thetas'].items()):
    ax = axes[i, 1]
    
    ### BULK WATER 
    h, x = np.histogram(pure_tip4pew['theta'], x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h, f'k', mfc='None', label='Bulk')

    h, x = np.histogram(pure_tip4pew['alpha'][pure_tip4pew['bonded']], x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h / 2.5, f'k--', mfc='None')

    
    h, x = np.histogram(d, x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h, f'C0-', mfc='None', label='LS')
        
    h, x = np.histogram(hs['pose_thetas'][key], x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h, f'C3', mfc='None', label='HS')
    
    
    ax = axes[i, 1]
    h, x = np.histogram(ls['pose_alphas'][key], x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h / 2.5, f'C0--', mfc='None')
        
    h, x = np.histogram(hs['pose_alphas'][key], x, density=True)
    ax.plot(x[1:] - 0.5 * dx, h / 2.5, f'C3--', mfc='None')
    
    
    

    #ax.legend(loc='best', fontsize=24)
    ax.set_xlim([50, xmax])
    ax.grid()
    if i < 3:
        ax.set_xticklabels([])
    ax.set_ylim([0, 0.0199])
    ax.set_ylabel('$\Gamma$ (deg.$^-1$)')
    ax.yaxis.set_label_position("right")
    ax.yaxis.tick_right()
    ax.set_xticks(np.arange(75, 200, 25))
    ax.set_xlim([51, 182])


    
ax.set_xlabel('$\\Theta$- and $\\alpha$-angles (deg.)')
axes[0, 1].legend(loc='best', frameon=False)
plt.tight_layout(h_pad=-0.1, w_pad=0.1)
plt.savefig('fig6.pdf', bbox_inches='tight', dpi=300)
plt.show()
