set size 1,1.1
set encoding iso_8859_1
set title "pose DBBB" font ",24" 
set output "pec-febpy3-4h2o-DBBB-b3lyp-opls-tip4pew.eps"
set terminal postscript eps enhanced color font ",24"
set ylabel "V (eV)" offset 1
set xlabel "Fe-O_{pose D} distance ({\305})"
set yrange [-0.85:0.6]
set xrange [3.5:9.0]
set xtics font ",26"
set ytics font ",26"
set format y "%1.1f"
set format x "%1.1f"
set arrow from 3.5,0 to 9.0,0 nohead 
plot "pec-febpy3-4h2o-DBBB-b3lyp.dat"        u 1:2 w l title "B3LYP LS" lt 5 lw 5 lc 3 ,\
     "pec-febpy3-4h2o-DBBB-b3lyp.dat"        u 1:3 w l title "B3LYP HS" lt 5 lw 5 lc 1 ,\
     "pec-febpy3-4h2o-DBBB-b3lyp.dat"        u 1:4 w l title "B3LYP-D3(BJ) LS" lt 1 lw 4 lc 3 ,\
     "pec-febpy3-4h2o-DBBB-b3lyp.dat"        u 1:5 w l title "B3LYP-D3(BJ) HS" lt 1 lw 4 lc 1 ,\
     "pec-febpy3-4h2o-DBBB-opls-tip4pew.dat" u 1:2 w l title "OPLS/TIP4PEW LS" lt 1 lw 4 lc 5 ,\
     "pec-febpy3-4h2o-DBBB-opls-tip4pew.dat" u 1:3 w l title "OPLS/TIP4PEW HS" lt 1 lw 4 lc rgb "salmon" 
