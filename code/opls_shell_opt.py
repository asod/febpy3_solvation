import numpy as np
from ase.io import write
from ase.calculators.tip4p import rOH, angleHOH
from ase.calculators.combine_mm import CombineMM
from ase.constraints import FixAtoms, FixBondLengths as RATTLE
from ase.optimize import FIRE
from tm_tools import reset_geom, DummyCalc, lj_fe_gs_opls as opls
from tip4pew import sigma0, epsilon0, TIP4PEW as TIP4P

''' Code used for relaxing OPLS/TIP4P water shell geometries.

    Starting from the B3LYP-D3(BJ) Geometries, resetting
    the internal water geometry to that of TIP4PEW. 

    Using the FIRE optimizer from ASE, with the standard 
    convergence criteria of Fmax <= 0.05 eV/Ang
'''

def rigid(atoms, qmidx):
    ''' only works for solute..solvent setups '''
    qm = qmidx[-1] + 1
    rattle = ([(3 * i + j, 3 * i + (j + 1) % 3)
                        for i in range((len(atoms) - qm + 1) // 3)
                        for j in [0, 1, 2]])

    rattle = [(c[0] + qm, c[1] + qm) for c in rattle]
    return rattle

def relax(atoms, these_charges, name='temp'):
    mol_idx = list(range(61))
    mol = atoms[mol_idx]

    # LJ Params
    sigs_mol = np.zeros(len(mol))
    epss_mol = np.zeros(len(mol))
    for i, el in enumerate(mol.get_chemical_symbols()):
        sigs_mol[i] = opls[el][1]
        epss_mol[i] = opls[el][0]

    atoms.calc = CombineMM(mol_idx, len(mol_idx), 3,
                           DummyCalc(these_charges),
                           TIP4P(),
                           sigs_mol,
                           epss_mol,
                           np.array([sigma0, 0, 0]),
                           np.array([epsilon0, 0, 0]),
                           rc=np.inf)

    atoms.constraints = tuple([FixAtoms(mol_idx)] + [RATTLE(rigid(atoms, mol_idx))])
    opt = FIRE(atoms, trajectory=f'{name}.traj',
                      logfile=f'{name}.log')
    opt.run(fmax=0.05)
    return atoms


charges = np.genfromtxt('../charges.dat',
                        skip_header=2, skip_footer=5)
q_ls_vac = charges[:, 1]
q_hs_vac = charges[:, 2]
q_ls_sol = charges[:, 4]
q_hs_sol = charges[:, 5]
charges = [np.vstack((q_ls_vac, q_hs_vac)), np.vstack((q_ls_sol, q_hs_sol))]
geoms = sorted(glob.glob('../geometries/febpy3-17-20h2o/*xyz'))


shells = ['17h2o', '18h2o', '19h2o', '20h2o']
envs = ['vac', 'sol']

for shell in shells:
    for m, mult in enumerate(['ls', 'hs']):
        for e, env in enumerate(envs):
            these_charges = charges[e][m, :]
            xyz = [g for g in geoms if (shell in g.split('/')[-1]) and (mult in g.split('/')[-1])]
            assert len(xyz) == 1
            these_atoms = read(xyz[0])
            o_idx = [atom.index for atom in these_atoms if atom.symbol == 'O']
            reset_atoms = reset_geom(these_atoms.copy(), o_idx)
            
            opath = f'../geometries/febpy3-17-20h2o/febpy3-{shell}_{mult}_opls_q-{env}'
            print(opath)
            opls_atoms = relax(reset_atoms, these_charges, opath)
            write(opath + '.xyz', opls_atoms, plain='True')
