''' Get all 3-body interaction energies from a solvent shell.

1. Create all 3mers from the shell
2. Loop over all 3mers in trajectory

    A. Calculate 3body term
    B. Calculate all 2body terms
    C. Calculate all 1body terms
    D. Calculate 3body interaction energy
    E. Update results with data from (A-D)

You can also do a single step N with do_step=N

See 10.1021/ct300913g or 10.1103/PhysRevA.76.013202 for details

'''

import itertools
import numpy as np
from ase.io import read
from ase.calculators.combine_mm import CombineMM
from ase.calculators.tip4p import TIP4P, sigma0, epsilon0
from ase.calculators.orca import ORCA
from tools import DummyCalc, lj_fe_gs_opls
import os
simpleinp = 'B3LYP D3 def2-TZVP TightSCF Slowconv'  
blocks = '%scf maxiter 1000 end %pal nprocs 16 end'

class Body3:

    def __init__(self, o_idx, r_max, charge=[2, 0, 0], mult=1, ftag='3body', mm_charges=None,
                 enable_qm=True, do_step=None, verbose=True, allghost=False, commondict=False):
        self.images = None
        self.o_idx = o_idx
        self.r_max = r_max
        self.ftag = ftag
        self.solute_idx = list(range(self.o_idx))
        self.charge = charge  # list of molecular charges of the 3 bodies
        self.mult = mult  # multiplicity of the system
        self.threebods = None
        self.tb_idxs = None
        self.do_qm = enable_qm
        self.do_step = do_step
        self.verbose = verbose
        self.allghost = allghost
        self.cd = commondict

        self.charges = mm_charges  # only for MM unit test of giving 0 3bd energy.
        self.sigs_solu = None
        self.epss_solu = None
        self.iter = None
    
    def run(self, shell):
        atoms = read(shell)
        self.span(atoms, self.o_idx, self.r_max)
        v3bs = np.zeros(len(self.tb_idxs))

        if self.do_step:
            self.tb_idxs = [self.tb_idxs[self.do_step]]
        else:
            self.do_step = 0

        ofile = self.ftag + '.energies'
        self.ofile_monos = self.ftag + '_monos.npy'
        self.ofile_dimer = self.ftag + '_dimer.npy'
        if self.cd:
            self.ofile_monos = self.cd + '_monos.npy'
            self.ofile_dimer = self.cd + '_dimer.npy'


        self.monosdict = {}
        self.dimerdict = {}
        if os.path.exists(self.ofile_monos):
            self.monosdict = np.load(self.ofile_monos, allow_pickle=True).item()
        if os.path.exists(self.ofile_dimer):
            self.dimerdict = np.load(self.ofile_dimer, allow_pickle=True).item()

        if os.path.exists(ofile):  # read in how far we got and start from there:
            with open(ofile, 'r') as f:
                lines = f.readlines()
            last_step = int(lines[-1].split()[0])
            print(f'Previous run exists, skipping until step: {last_step}')
        else:
            last_step = -1

        hdr = 'ENERGIES:\n'
        hdr += '#     TOTAL             E2B1            E2B2            E2B3            E1B1            E1B2            E1B3          V3B\n'
        if last_step < 0:
            with open(ofile, 'a') as f:
                f.write(hdr)
            if self.verbose:
                print(hdr)
        for ta, widxs in enumerate(self.tb_idxs):
            if ta <= last_step:
                continue

            # do_step is 0 if all steps are taken, or N if tb_idxs are truncated,
            # so the same indexing is kept
            self.iter = ta + self.do_step
            # Make masks used to divide up atoms for the various terms
            all_masks = [np.zeros(len(atoms), bool) for x in range(len(widxs) + 1)]
            # solute is always the first:
            all_masks[0][self.solute_idx] = True
            # then the rest of the monomers
            for w, widx in enumerate(widxs):
                all_masks[w + 1][widx] = True
            self.all_masks = np.stack(all_masks, axis=0)

            etot = self.calc_total(atoms)

            # generalize all this into a for loop for n-body at some point...
            # 2body terms
            e2bs = self.calc_v2b(atoms)

            # 1 body
            e1bs = self.calc_v1b(atoms)

            # 3 Body interaction energy:
            v3bs[ta] = etot - sum(e2bs) + sum(e1bs)

            data = f'{self.iter:03d} {etot:16.6f}' +\
                       ''.join(f'{x:16.6f}' for x in e2bs) +\
                       ''.join(f'{x:16.6f}' for x in e1bs) +\
                       f'{v3bs[ta]:16.6f}'
            if self.verbose:
                print(data)
            with open(ofile, 'a') as f:
                f.write(data + '\n')
            # if commondict is used, we're probably splitting up the calcs, so we
            # so lets save individual npys of the total data of each run as well. 
            if self.cd:  
                idx = tuple(map(tuple, self.tb_idxs[0]))
                od = {}
                od[idx] = (v3bs, e2bs, e1bs)
                np.save(self.ftag + '_v3b.npy', od)

        return v3bs

    def calc_v1b(self, atoms):
        ''' Monomer energies, CP corrected'''
        e1bs = np.zeros(3)
        # ORCA: use ALL atoms (of the 3 monomers), but tag the ghost atoms.
        total_mask = np.any(self.all_masks, axis=0)  # for "... of the monomers"
        for m, mask in enumerate(self.all_masks):
            mono = atoms.copy()
            tags = np.zeros(len(atoms)) + 71  # 71 means ghost in ORCA
            tags[mask] = 0
            mono.set_tags(tags)
            if not self.allghost:
                mono = mono[total_mask]  # ... of the monomers. Not the other solvent shell mols.

            this_ftag = self.ftag + f'{self.iter:04d}_v1b{m:02d}'
            contains_solute = (mask & self.all_masks[0]).any()
            if contains_solute:
                mult = self.mult
            else:
                mult = 1
            if not self.do_qm:
                continue
            mono.calc = ORCA(label=this_ftag,
                             charge=int(self.charge[m]),
                             mult=mult,
                             orcasimpleinput=simpleinp,
                             orcablocks=blocks,
                             task='energy')

            key = tuple(np.where(mask)[0])
            if (key in self.monosdict.keys()) and (self.allghost):
                print('reusing mono!')
                e1bs[m] = self.monosdict[key]
            else:
                e1bs[m] = mono.get_potential_energy()
                self.monosdict[key] = e1bs[m]
        
            np.save(self.ofile_monos, self.monosdict)

        return e1bs

    def calc_v2b(self, atoms):
        ''' Calculate 2body terms.
            If ever needed, this is pretty easily generalizable by changing the "2" to
            "n" in the itertools call.... basically?
        '''
        # 2 Body terms, 3 of those:
        e2bs_opls = np.zeros(3)
        e2bs = np.zeros(3)
        for c, c2 in enumerate(itertools.combinations(self.all_masks, 2)):
            dimer_msk = np.any(c2, axis=0)
            this_dimer = atoms[dimer_msk]
            contains_solute = (dimer_msk & self.all_masks[0]).any()
            if contains_solute:
                this_dimer.calc = CombineMM(self.solute_idx,
                                            len(self.solute_idx), 3,
                                            DummyCalc(self.charges),
                                            TIP4P(rc=np.inf),
                                            self.sigs_solu, self.epss_solu,
                                            np.array([sigma0, 0, 0]),
                                            np.array([epsilon0, 0, 0]),
                                            rc=np.inf)
            else:
                this_dimer.calc = TIP4P(rc=np.inf)

            e2bs_opls[c] = this_dimer.get_potential_energy()

            ### And now for the orca:
            total_mask = np.any(self.all_masks, axis=0)
            dimer = atoms.copy()
            tags = np.zeros(len(atoms)) + 71  # start with everything is ghost
            tags[dimer_msk] = 0  # these are the real atoms
            dimer.set_tags(tags)
            if not self.allghost:
                # BUT cut out all other atoms than the 3 monomers in total:
                dimer = dimer[total_mask]
            this_ftag = self.ftag + f'_{self.iter:04d}_v2b{c:02d}'
            if contains_solute:
                charge = self.charge[0]
                mult = self.mult
            else:
                charge = 0
                mult = 1
            dimer.calc = ORCA(label=this_ftag,
                              charge=int(charge),
                              mult=mult,
                              orcasimpleinput=simpleinp,
                              orcablocks=blocks,
                              task='energy')

            if self.do_qm:
                key = tuple(np.where(dimer_msk)[0])
                if (key in self.dimerdict.keys()) and (self.allghost):
                    print('reusing dimer!')
                    e2bs[c] = self.dimerdict[key]
                else:
                    e2bs[c] = dimer.get_potential_energy()
                    self.dimerdict[key] = e2bs[c]
            else:
                e2bs[c] = e2bs_opls[c]

            np.save(self.ofile_dimer, self.dimerdict)

        return e2bs

    def calc_total(self, atoms):
        ''' total energy of system comprised of the 3 monomers:
            E(1, 2, 3)
        '''
        total_mask = np.any(self.all_masks, axis=0)

        # OPLS as a check
        sigs_solu = np.zeros(len(self.solute_idx))
        epss_solu = np.zeros(len(self.solute_idx))
        for i, el in enumerate(atoms[self.all_masks[0]].get_chemical_symbols()):
            sigs_solu[i] = lj_fe_gs_opls[el][1]
            epss_solu[i] = lj_fe_gs_opls[el][0]

        self.sigs_solu = sigs_solu
        self.epss_solu = epss_solu

        ### Total energy of system:
        these_atoms = atoms[total_mask]
        these_atoms.calc = CombineMM(self.solute_idx,
                                     len(self.solute_idx), 3,
                                     DummyCalc(self.charges),
                                     TIP4P(np.inf),
                                     sigs_solu, epss_solu,
                                     np.array([sigma0, 0, 0]),
                                     np.array([epsilon0, 0, 0]),
                                     rc=np.inf)

        etot = these_atoms.get_potential_energy()
        self.etot_opls = etot

        ### The actual DFT calculation, via orca
        this_ftag = self.ftag + f'_{self.iter:04d}_total'

        # reset these_atoms so you can get the ENTIRE basis of the shell if
        # needed
        if not self.allghost:
            these_atoms = atoms[total_mask]
        else:
            tags = np.zeros(len(atoms)) + 71  # start with everything is ghost
            tags[total_mask] = 0  # these are the real atoms
            these_atoms = atoms.copy()
            these_atoms.set_tags(tags)

        if self.do_qm:
            these_atoms.calc = ORCA(label=this_ftag,
                                    charge=int(sum(self.charge)),
                                    mult=self.mult,
                                    orcasimpleinput=simpleinp,
                                    orcablocks=blocks,
                                    task='energy')

            etot = these_atoms.get_potential_energy()

        return etot

    def span(self, atoms, o_idx, r_max):
        ''' 1. Create all 3mers from the shell:
        atoms: ASE Atoms object of the entire system
        o_idx: Index of first water oxygen
        r_min: The maximum distance the Oxygens are allowed
            to be away from each other in order to be included
            in the final trajectory
        '''
        atoms.constraints = []
        waters = [list(range(i, i + 3)) for i in range(o_idx, len(atoms), 3)]
        all_water_combs = list(itertools.combinations(waters, 2))
        solute = atoms[:o_idx]
        threebods = []
        tb_idxs = []
        for awc in all_water_combs:
            waterpair = sum(map(list, awc), []) 
            waters = atoms[waterpair]
            if waters.get_distance(0, 3) < r_max:
                threebods.append(solute + waters)
                tb_idxs.append(awc)
        self.threebods = threebods
        self.tb_idxs = tb_idxs
