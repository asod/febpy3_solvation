import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
from scipy.optimize import linear_sum_assignment
import rmsd
from ase import Atoms, units
from ase.io.cube import write_cube
from ase.io.trajectory import TrajectoryWriter, Trajectory
from tqdm.auto import tqdm
from MDAnalysis.transformations import unwrap, wrap, center_in_box
from MDAnalysis.analysis import distances

def ang(v1, v2):
    ''' Robust angle calc '''
    cosang = np.dot(v1, v2)
    sinang = np.linalg.norm(np.cross(v1, v2))
    return np.degrees(np.arctan2(sinang, cosang))

def water_dimer_identify(mol1, mol2, i1=0, i2=0):
    ''' Takes two water Hbonded molecules and identifies which is the donor
        and which is the acceptor.

        mol1 : Atoms obj. Water1. OHH sequence
        mol2 : Atoms obj. Water2. OHH sequence

        i1, i2 : int. Original indices of O in mol1 and mol2, respectively.
    '''
    atoms = mol1 + mol2
    h_dists = [np.linalg.norm(atoms[y - x].position - atoms[x].position)
                for x in [0, 3] for y in [4, 5]]
    short_h = np.argmin(h_dists)
    hdonor_idx = [4, 5, 1, 2][short_h]

    if hdonor_idx > 3:  # mol2 was donor
        od_idx = 3
        don_idx = i2
        hd_new = hdonor_idx - 3

    else:  # mol1 was donor
        od_idx = 0
        hd_new = hdonor_idx
        don_idx = i1
    mask = np.zeros(6, bool)
    mask[od_idx:od_idx + 3] = True
    donor = atoms[mask]
    acceptor = atoms[~mask]

    # return donor, acceptor, and new H donor idx
    return donor, acceptor, hd_new, don_idx


def get_beta(atoms, acceptor_idx=3, h_idx=1):
    ''' Beta angle. See e.g 10.1021/acs.jctc.9b00778 '''
    # Assuming OHHOHH sequence.
    aidx = acceptor_idx
    didx = abs(aidx - 3)
    pos = atoms.get_positions()
    V_OO = pos[aidx] - pos[didx]
    V_OH = pos[h_idx] - pos[didx]
    for v in (V_OO, V_OH):
        v /= np.linalg.norm(v)

    ooh = np.arctan2(np.linalg.norm(np.cross(V_OH, V_OO)), np.dot(V_OH, V_OO))
    # O, O, V_A x V_a plane, to define directionality
    # and to get rotation axis
    vHa = pos[didx] - pos[didx + 1]
    vHb = pos[didx] - pos[didx + 2]
    rot = np.cross(vHa, vHb) # normal of HH plane: Rotation axis
    rot /= np.linalg.norm(rot)

    normal = np.cross(rot, V_OO)
    normal /= np.linalg.norm(normal)

    # Directionality:
    X = np.dot(atoms[h_idx].position - pos[didx], normal)
    if X == 0:
        X = 1
    else:
        X /= np.abs(X)

    return X * np.degrees(ooh), rot

def get_roo(mol1, mol2):
    return np.linalg.norm(mol1[0].position - mol2[0].position)

def get_alpha(mol1, mol2, h):
    ''' Alpha angle. See e.g 10.1021/acs.jctc.9b00778 '''
    pos = (mol1 + mol2).get_positions()
    v1 = pos[0] - pos[h]
    v1 /= np.linalg.norm(v1)
    v2 = pos[3] - pos[h]
    v2 /= np.linalg.norm(v2)

    return ang(v1, v2)


class Sheller:
    '''
        3D Histograms of solvent densities around aligned solutes.

        Parameters
        -----------
        universe :   MDAnalysis Universe object.
                     Universe with trajectory to sample from.

        name:       str
                    name of output cube files
        solutesel:  str
                    MDAnalysis selection string of solute atoms to align to

        Usage example:

        <load your trajectory into an MDAnalysis universe u>
        >>> shl = Sheller(u, name='my_fancy_binning', solutesel='resid 1:7')
        >>> shl.make_array(watersel='type OW')
        >>> O, Oedges = shl.bin3d(shl.v_pos, bins=(151, 151, 151), lim=9)

    '''
    def __init__(self, universe, name='febpyshell', solutesel='resid 1:4'):
        self.u = universe
        self.name = name
        self.solusel = solutesel

        # frame to align to: Solute in frame 0 of the universe:
        if universe != None:
            solute = universe.select_atoms(solutesel)
            self.center = rmsd.centroid(solute.positions)
            self.ref_frame = solute.positions - self.center

    def make_array(self, watersel='type OW or type HW', start=0, end=-1, step=1):
        u = self.u
        water = u.select_atoms(watersel)
        solute = u.select_atoms(self.solusel)

        v_pos = np.zeros((len(u.trajectory[start:end:step]), len(water), 3))
        u_pos = np.zeros((len(u.trajectory[start:end:step]), len(solute), 3))
        for f, frame in tqdm(enumerate(u.trajectory[start:end:step])):
            this_vpos = water.positions
            this_upos = solute.positions

            ali_u, ali_v = self.align(this_upos, this_vpos)
            v_pos[f, :, :] = ali_v
            u_pos[f, :, :] = ali_u

        self.v_pos = v_pos
        self.u_pos = u_pos

    def make_array_from_asetraj(self, trajfile, idx, which='O', start=0, end=-1, step=1):
        ''' For 3D histogramming of already aligned, cut ASE trajectories '''
        trajectory = Trajectory(trajfile, 'r')
        atoms = trajectory[0]
        water = atoms[idx::3]

        v_pos = np.zeros((len(trajectory[start:end:step]), len(water), 3))
        u_pos = np.zeros((len(trajectory[start:end:step]), idx, 3))
        for f, frame in tqdm(enumerate(trajectory[start:end:step])):
            if which == 'O':
                this_vpos = atoms[idx::3] 
            elif which == 'H':
                this_vpos = atoms[idx + 1::3] + atoms[idx + 2::3] 
            this_upos = atoms[:idx].positions
            v_pos[f, :, :] = this_vpos.positions
            u_pos[f, :, :] = this_upos

        self.v_pos = v_pos
        self.u_pos = u_pos

    def align(self, solu_pos, solv_pos):
        this_cen = rmsd.centroid(solu_pos)
        centered_v = solv_pos - this_cen
        centered_u = solu_pos - this_cen
        # Get rotmat from solute
        U = rmsd.kabsch(self.ref_frame, centered_u)
        # Apply it
        aligned_u = np.dot(centered_u, U.T)
        aligned_v = np.dot(centered_v, U.T)

        return aligned_u, aligned_v

    def bin3d(self, pos, bins=(81, 81, 81), normed=True, lim=15):
        ''' 3D bins the (Nwatermol x Nframes, 3) array "pos"
            The center atom (e.g. Fe in FeBPY needs to be
            in (0, 0, 0) ).

            The atoms in the cube will be the avg aligned positions
            of the solute.

        '''

        flatpos = pos.reshape(-1, 3)
        name = self.name

        solute_elements = self.u.select_atoms(self.solusel).elements
        mean_solu_pos = np.mean(self.u_pos, axis=0)
        atoms = Atoms(''.join(el for el in solute_elements),
                      positions=mean_solu_pos)
        blim = lim / units.Bohr
        atoms.set_cell((blim, blim, blim))
        atoms.translate([blim, blim, blim])

        self.atoms = atoms

        O, Oedges = np.histogramdd(flatpos,
                                  bins=bins,
                                  range=((-lim, lim),(-lim, lim),(-lim, lim)), density=normed)

        with open(name + '.cube', 'w') as f:
            write_cube(f, atoms, data=O, origin=np.array([blim / 2, blim / 2, blim / 2]))

        return O, Oedges

    def cut2ase(self, centersel=None, watersel='name O',
                start=0, end=-1, step=1, nlim=17, rlim=10):
        ''' Make an ASE trajectory of the aligned frames

            Cut out only the nlim first waters within rlim, align, save
        '''
        u = self.u

        if centersel is not None:
            center = u.select_atoms(centersel)
        else:
            center = u.select_atoms(self.solusel)

        ag = u.atoms
        # rewrap around center:
        workflow = (unwrap(ag),
                   center_in_box(center, center='mass'),
                   wrap(ag, compound='fragments'))
        u.trajectory.add_transformations(*workflow)

        tw = TrajectoryWriter(f'{self.name}_cut-n{nlim}-r{rlim}.traj')

        total = len(u.trajectory[start:end:step]) 
        for f, frame in tqdm(enumerate(u.trajectory[start:end:step]), total=total): 
                # Get water-oxygens around center, but not potential Os in solute:
                first_shell = u.select_atoms(f'not {self.solusel} and ({watersel} and (around {rlim} {centersel}))', updating=True)
                c_com = center.center_of_mass(compound='residues')
                d = distances.distance_array(c_com, first_shell.positions)[0]
                com_idx = np.argsort(d)
                closest_residues = [first_shell[cidx].resid for cidx in com_idx[:nlim]]
                whole_solv = ' '.join([f'resid {cr} or' for cr in closest_residues])[:-3]

                ns = ag.select_atoms(self.solusel)
                ws = ag.select_atoms(whole_solv + ' and not name EPW')  # WIP: flexible filtering out EPs

                ali_u, ali_v = self.align(ns.positions, ws.positions)

                elements = (ns + ws).elements

                atoms = Atoms(''.join(el for el in elements), positions=np.vstack((ali_u, ali_v)))
                tw.write(atoms)


class WaterInsight:
    '''
        Linear sum assignment (LSA)-based reindexing of MM waters to compare with some reference.
        Solvation shell stuff. Some functionality is not generalized from org. use case.

        will save an npy file with the data.

        Parameters
        ----------------
        reference_atoms:     Atoms obj
                             Providing reference-coordinates for the linear sum assignment

        solu_idx:            int
                             number of atoms in the solute.

        nmol:                int
                             number of solvent molecules

        out_container:       dict
                             for categorizing distances and angles based on the LSA-based solvent
                             molecule categories

        name:                str
                             name of output npy

        within:              float
                             Wernet criterium H-bond distance & limit for how long waters can be moved
                             by the linear sum assignment before they are classified as unlabeled

        Usage example:


        <assuming you have some reference atoms 'ref' of pose 'A', 'B', C', 'D', which are categorized in 'qm17_poses'>
        >>> qm17_poses = {88: 'D', 79: 'B', 82: 'B', 85: 'B', 100: 'C',  # format: {ref index: category}
                          97: 'C', 106: 'C', 67: 'A', 61: 'A', 64: 'A',
                          109: 'C', 103:'C', 94: 'C', 76: 'B', 73: 'B', 70: 'B', 91: 'D'}

        >>> wi = WaterInsight(reference_atoms=ref, solu_idx=61, nmol=17, out_container=qm17_poses)
        >>> traj = Trajectory('some_trajectory_generated_with_the_Sheller_class.traj')  # ASE trajectory.
        >>> wi.sample(traj)
    '''

    def __init__(self, reference_atoms, solu_idx, nmol, out_container=None, name='febpyshell_analysis', within=3.4) -> None:
        self.within = within
        self.ref = reference_atoms
        self.solu_idx = solu_idx
        self.name = name
        self.nmol = nmol

        if out_container is None:
            out_container = {'Unlabeled': []}
        self.oc = out_container  # If you want to split up your output based on labels
                                 # of the reference structure.

    def linear_sum_reassign(self, refpos, newpos):
        ''' Get map that reassigns newpos to be as close as possible to refpos '''
        C = cdist(refpos, newpos)
        _, assignment = linear_sum_assignment(C)
        return assignment

    def sample(self, traj):
        atoms = self.ref[self.solu_idx:]  # REFERENCE WATERS - INCLUDING H
        mask = [atom.symbol != 'O' for atom in atoms]  # for masked arrays
        su = self.solu_idx

        total_moved_too_far = 0
        total_hbonds = 0

        entries = np.unique([val for val in self.oc.values()])
        pose_feos = {entry: [] for entry in entries}
        pose_feos['Unlabeled'] = []
        pose_alphas = {entry: [] for entry in entries}
        pose_thetas = {entry: [] for entry in entries}

        hbonds_per_frame = np.zeros(len(traj))

        for f, frame in enumerate(tqdm(traj)):
            # Reassign only on Os
            assignment = self.linear_sum_reassign(atoms[::3].get_positions(), frame[su::3].get_positions())
            # Get
            w_map = np.concatenate([list(range(3 * x + su, 3 * x + 3 + su)) for x in assignment])  # get back Hs

            reas_waters = frame[w_map]
            oidx = [atom.index for atom in reas_waters if atom.symbol == 'O']

            w_qm_then_mm = atoms + reas_waters
            allOpos = np.vstack([atom.position for atom in w_qm_then_mm if atom.symbol =='O'])
            has_moved = np.linalg.norm(allOpos[:self.nmol, :] - allOpos[self.nmol:, :], axis=1)

            #assignable_idxs = np.where(has_moved <= within)[0]
            # check if they:
            # 1. Havent moved too far
            # 2. Count Wernet Hbonds
            # 3. Measure alpha angles
            # 4. Measure OO distances
            bond_idxs = []
            for i, o in enumerate(oidx):
                r_feo = r_feo = np.linalg.norm(frame[0].position - reas_waters[o].position)    # Needs generalization
                if has_moved[i] > self.within:  #has moved too far.
                    total_moved_too_far += 1
                    pose_feos['Unlabeled'].append(r_feo)
                    continue
                # We define every waters that did NOT move more as "good" to be pose-classified.
                # So we can put in the distances now:
                pose_feos[self.oc[o + su]].append(r_feo)


                # Get all W-W distances.
                dists = reas_waters.get_distances(o, list(range(len(reas_waters))))
                dists[dists == 0] += 1e6  # remove self-distance
                w_idx = np.ma.where(np.ma.MaskedArray(dists, mask, fill_value=1e6) <= self.within)[0]
                mol1 = reas_waters[list(range(o, o + 3))]
                for j in w_idx:
                    mol2 = reas_waters[range(j, j + 3)]
                    (don, acc, hd, don_idx) = water_dimer_identify(mol1, mol2, o, j)
                    beta = np.abs(get_beta(don + acc, 3, hd)[0])
                    roo = get_roo(don, acc)
                    bonded = roo < self.within - 0.00044 * (beta)**2
                    if bonded:
                        bond_idxs.append((o + su, j + su))
                    alpha = get_alpha(don, acc, hd)
                    pose_alphas[self.oc[don_idx + su]].append(alpha)

                    theta = [(don + acc).get_angle(hd, 3, x) for x in [4, 5]]
                    pose_thetas[self.oc[don_idx + su]].append(theta)

            uni = []
            for b in bond_idxs:
                if (b[0], b[1]) not in uni and (b[1], b[0]) not in uni:
                    uni.append(b)
            total_hbonds += len(uni)
            hbonds_per_frame[f] = len(uni)

        out = {'total_moved_too_far': total_moved_too_far,
               'total_hbonds': total_hbonds,
               'pose_feos': pose_feos,
               'pose_alphas': pose_alphas,
               'pose_thetas': pose_thetas}
        np.save(self.name + '.npy', out)

    def histogram(self, data, xmin, xmax, xlabel, unit, nbins=100, ax=None,
                  **kwargs):
        if ax is None:
            fig, ax = plt.subplots(1, 1, figsize=(9, 5))
        x = np.linspace(xmin, xmax, nbins)
        dx = x[1] - x[0]
        for i, (key, d) in enumerate(data.items()):
            h, x = np.histogram(d, x, density=True)
            if len(kwargs) == 0:
                ax.plot(x[1:] - 0.5 * dx, h, f'C{i}o-', mfc='None', label=f'{key}')
            else:
                ax.plot(x[1:] - 0.5 * dx, h, color=f'C{i}', label=f'{key}', **kwargs)

            ax.legend(loc='best', fontsize=18)

        ax.set_xlabel(f'{xlabel} {unit}')
        ax.set_ylabel(f'$\Gamma$({xlabel}) (1 / {unit})')
        ax.set_xlim([xmin - 0.05 * xmin, xmax + 0.05 * xmax])
        return ax
