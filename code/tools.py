import numpy as np
from ase import units
from ase.calculators.calculator import Calculator
from tip4pew import TIP4PEW, sigma0, epsilon0
from ase.calculators.combine_mm import CombineMM
from ase import Atoms


trslate = {'BLYP':'BLYP',
           'BLYPD3':'BLYP',
           'BLYPT4P':'BLYP',
           'BLYPD3T4P':'BLYP',
           'BP86':'GGA_X_B88+GGA_C_P86',
           'BP86T4P':'GGA_X_B88+GGA_C_P86',
           'BP86D3':'GGA_X_B88+GGA_C_P86',
           'BP86D3T4P':'GGA_X_B88+GGA_C_P86'}

trslate_d3 = {'BLYPD3':'b-lyp',
              'BLYPD3T4P':'b-lyp',
              'BP86D3':'b-p',
              'BP86D3T4P':'b-p'}

trslate_orca = {'BLYP':'BLYP',
                'BLYPD3': 'BLYP D3BJ',
                'BP86':'BP86',
                'BP86D3':'BP86 D3BJ',
                'BLYPT4P':'BLYP',
                'BLYPD3T4P': 'BLYP D3BJ',
                'BP86T4P':'BP86',
                'BP86D3T4P':'BP86 D3BJ',
                'B3LYPD3': 'B3LYP D3BJ RIJCOSX'}


class DummyCalc(Calculator):
    implemented_properties = ['energy', 'forces']

    def __init__(self, charges):
        ''' charge carrier for the mol '''

        self.charges = charges
        self.sites_per_mol = len(charges)
        Calculator.__init__(self)

    def get_virtual_charges(self, atoms):
        return self.charges

    def add_virtual_sites(self, positions):
        return positions

    def redistribute_forces(self, forces):
        return forces

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        energy = 0.0
        forces = np.zeros_like(atoms.get_positions())

        self.results['energy'] = energy
        self.results['forces'] = forces



def reset_geom(atoms, o_idx):
    ''' reset water molecule to TIP4PEW geometry '''
    def set_ohs(atoms, o):
        pos = atoms.get_positions()
        for h in [1, 2]:
            v = pos[o + h] - pos[o]
            v /= np.linalg.norm(v)
            pos[o + h] = pos[o] + v * rOH

        atoms.set_positions(pos, apply_constraint=False)

    for o in o_idx:
        atoms.set_angle(o + 1, o, o + 2, angleHOH)
        set_ohs(atoms, o)

    return atoms



# -------------------------------------Fe(bpy)3, OPLS:
C_eps = 0.00329567
C_sig = 3.55000
N_eps = 0.00737190
N_sig = 3.25000
Fe_eps = 0.00056373
Fe_sig = 2.59400
H_eps = 0.00130092
H_sig = 2.42000

lj_fe_gs_opls = {'C':  (C_eps, C_sig),
                'N':  (N_eps, N_sig),
                'Fe': (Fe_eps, Fe_sig),
                'H': (H_eps, H_sig)}


def get_energy(atoms, mask, charges, mol_size=61, lj=None):
    '''  Get MM (=TIP4P coulomb and OPLS LJ) interaction energy between:
         [Complex+n Waters] & [Another water]

         Needs to add TIP4P charge sites to waters now included in first
         'monomer'. Rearranges those waters, after getting positions and
         charges, such that the atoms-sequence will be mono1..mono2

         atoms:        entire Atoms object
         mask:         bool, True means mono1, so False is the scanning water
         charges:      charges of the complex
         mol_size:     number of atoms in the complex only.

         NB: You need to modify ASEs combine_mm calculator to save the coulomb
             and lj energy separately, it doesn't do this in the main branch.

    '''
    mask_mol = np.zeros(len(atoms), bool)
    mask_mol[:mol_size] = True
    mask_wat = np.ones(len(atoms), bool)
    mask_wat[:mol_size] = False

    # We need to manually add charge sites to the non-scanning waters
    # because they will be in the DummyCalc part of the system in CombineMM
    mask_add = mask_wat & mask

    add = atoms[mask_add]
    tip4p = TIP4PEW()
    pos = tip4p.add_virtual_sites(add.positions)
    chg = tip4p.get_virtual_charges(add)
    new = Atoms('OHHHe' * (len(pos) // 4), positions=pos)

    ### add up new object where the 'MM' part is last, to make it easier
    mono1 = atoms[mask_mol] + new
    mono2 = atoms[~mask]
    final =  mono1 + mono2

    # LJ stuff
    # add 0 LJ on the virtual charge site (He atoms)
    opls = lj_fe_gs_opls
    if lj:
        opls = lj
    tip4p_lj = {'H': (0, 0),
                'He':(0, 0),
                'O': (epsilon0, sigma0) }

    sigs_mol = np.zeros(len(mono1))
    epss_mol = np.zeros(len(mono1))
    for i, el in enumerate(mono1.get_chemical_symbols()):
        if i >= mol_size:
            sigs_mol[i] = tip4p_lj[el][1]
            epss_mol[i] = tip4p_lj[el][0]
        else:
            sigs_mol[i] = opls[el][1]
            epss_mol[i] = opls[el][0]


    # charges
    new_chg = np.concatenate((charges, chg))

    mol_idx = list(range(len(mono1)))
    final.calc = CombineMM(mol_idx, len(mol_idx), 3,
                           DummyCalc(new_chg),
                           TIP4PEW(),
                           sigs_mol, epss_mol,
                           np.array([sigma0, 0, 0]),
                           np.array([epsilon0, 0, 0]),
                           rc=np.inf)

    e_tot = final.get_potential_energy()
    if hasattr(final.calc, 'e_lj'):
        e_lj = final.calc.e_lj  # XXX These needs to be added in the combine_mm calc in ase.calculators.combine_mm
        e_es = final.calc.e_es
    else:
        e_lj = 0 
        e_es = 0

    return e_tot, e_es, e_lj
